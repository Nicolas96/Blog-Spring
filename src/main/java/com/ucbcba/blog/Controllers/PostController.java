package com.ucbcba.blog.Controllers;

import com.ucbcba.blog.Entities.Comment;
import com.ucbcba.blog.Entities.Post;
import com.ucbcba.blog.Services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.Set;

/**
 * Product controller.
 */
@Controller
public class PostController {

    @RequestMapping(value = "/",method = RequestMethod.GET)
    public String hello(Model model)
    {
        model.addAttribute("posts", postService.listAllPosts());
        return "Inicio";
    }

    private CommentService commentService;
    private PostService postService;
    private CategoryService categoryService;
    private UserService userService;

    @Autowired
    public void setPostService(PostService postService){this.postService=postService;}

    @Autowired
    public void setCategoryService(CategoryService categoryService){this.categoryService = categoryService;}

    @Autowired
    public void setUserService(UserService userService){this.userService = userService;}

    @Autowired
    public void setCommentService(CommentService commentService){this.commentService = commentService;}


    @RequestMapping(value = "/Posts" , method = RequestMethod.GET)
    public String list(Model model)
    {
        model.addAttribute("posts", postService.listAllPosts());
        return "Posts";
    }
    @RequestMapping(value = "/Post/new", method = RequestMethod.GET)
    public String newPost(Model model) {
        model.addAttribute("post", new Post());
        model.addAttribute("categories", categoryService.listAllCategories());
        model.addAttribute("users", userService.listAllUsers());
        return "PostForm";
    }

    @RequestMapping(value = "/Post", method = RequestMethod.POST)
    public String save(@Valid Post post, BindingResult bindingResult,  Model model) {
        if(bindingResult.hasErrors()){
            model.addAttribute("categories" , categoryService.listAllCategories());
            model.addAttribute("users", userService.listAllUsers());
            return "PostForm";
        }
        postService.savePost(post);
        return "redirect:/Posts";
    }

    @RequestMapping(value = "/Post/{id}", method = RequestMethod.GET)
    public String showPost(@PathVariable Integer id, Model model) {
        Post post = postService.getPostById(id);
        model.addAttribute("post", post);
        model.addAttribute("users",userService.listAllUsers());
        model.addAttribute("comment", new Comment(post));
        return "Post";
    }


    @RequestMapping(value = "/Post/editar/{id}", method = RequestMethod.GET)
    public String editPost(@PathVariable Integer id, Model model) {
            model.addAttribute("post", postService.getPostById(id));
            model.addAttribute("categories", categoryService.listAllCategories());
            model.addAttribute("users", userService.listAllUsers());
        return "PostForm";
    }

    @RequestMapping(value = "/Post/eliminar/{id}", method = RequestMethod.GET)
    public String deletePost(@PathVariable Integer id, Model model) {
        Post post = postService.getPostById(id);
        Set<Comment> comments = post.getComments();
        for (Comment comment: comments) {
            commentService.deleteComment(comment.getId());
        }
        postService.deletePost(id);
        return "redirect:/Posts";
    }

    @RequestMapping(value = "/Post/like/{id}", method = RequestMethod.GET)
    public String like(@PathVariable Integer id, Model model)
    {
        Post post = postService.getPostById(id);
        post.setLikes(post.getLikes() + 1 );
        postService.savePost(post);
        return "redirect:/Post/" + post.getId();
    }
    @RequestMapping(value = "/Post/dislike/{id}", method = RequestMethod.GET)
    public String dislike(@PathVariable Integer id, Model model)
    {
        Post post = postService.getPostById(id);
        if(post.getLikes() == 0)
        {
            return "redirect:/Post/" + post.getId();
        }
        post.setLikes(post.getLikes() - 1 );
        postService.savePost(post);
        return "redirect:/Post/" + post.getId();
    }

}
