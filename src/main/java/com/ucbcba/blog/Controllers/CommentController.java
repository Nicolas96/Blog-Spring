package com.ucbcba.blog.Controllers;

import com.ucbcba.blog.Entities.Comment;
import com.ucbcba.blog.Entities.Post;
import com.ucbcba.blog.Services.CommentService;
import com.ucbcba.blog.Services.PostService;
import com.ucbcba.blog.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.naming.Binding;
import javax.validation.Valid;

@Controller
public class CommentController {

    private CommentService commentService;
    private PostService postService;
    private UserService userService;

    @Autowired
    public void setCommentService(CommentService commentService){this.commentService = commentService;}

    @Autowired
    public void setPostService(PostService postService){this.postService=postService;}

    @Autowired
    public void setUserService(UserService userService){this.userService=userService;}


    @RequestMapping(value = "/comment", method = RequestMethod.POST)
    public String save(@Valid Comment comment , BindingResult bindingResult, Model model)
    {

        if(bindingResult.hasErrors()) {
            Post post = postService.getPostById(comment.getPost().getId());
            model.addAttribute("post",post);
            model.addAttribute("users",userService.listAllUsers());
            return "Post" ;
        }
        commentService.saveComment(comment);
        return "redirect:/Post/" + comment.getPost().getId();
    }

    @RequestMapping(value = "/Comment/like/{id}", method = RequestMethod.GET)
    public String like(@PathVariable Integer id, Model model)
    {
        Comment comment = commentService.getCommentById(id);
        comment.setLikes(comment.getLikes() + 1 );
        commentService.saveComment(comment);
        return "redirect:/Post/" + comment.getPost().getId();
    }
    @RequestMapping(value = "/Comment/dislike/{id}", method = RequestMethod.GET)
    public String dislike(@PathVariable Integer id, Model model)
    {
        Comment comment = commentService.getCommentById(id);
        if(comment.getLikes() == 0)
        {
            return "redirect:/Post/" + comment.getPost().getId();
        }
        comment.setLikes(comment.getLikes() - 1 );
        commentService.saveComment(comment);
        return "redirect:/Post/" + comment.getPost().getId();
    }
}
