package com.ucbcba.blog.Repositories;

import com.ucbcba.blog.Entities.User;
import org.springframework.data.repository.CrudRepository;
import javax.transaction.Transactional;

@Transactional
public interface UserRepository extends CrudRepository<User, Integer> {
}
