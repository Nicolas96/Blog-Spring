package com.ucbcba.blog.Repositories;

import com.ucbcba.blog.Entities.Comment;

import org.springframework.data.repository.CrudRepository;
import javax.transaction.Transactional;

@Transactional
public interface CommentRepository extends CrudRepository<Comment, Integer> {
}
