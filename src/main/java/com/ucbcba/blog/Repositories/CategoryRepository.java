package com.ucbcba.blog.Repositories;

import com.ucbcba.blog.Entities.Category;
import org.springframework.data.repository.CrudRepository;
import javax.transaction.Transactional;

@Transactional
public interface CategoryRepository extends CrudRepository<Category, Integer>{
}
