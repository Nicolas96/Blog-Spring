package com.ucbcba.blog.Repositories;

import com.ucbcba.blog.Entities.Post;
import org.springframework.data.repository.CrudRepository;
import javax.transaction.Transactional;

@Transactional
public interface PostRepository extends CrudRepository<Post, Integer> {
}
