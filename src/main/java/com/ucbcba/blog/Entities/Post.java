package com.ucbcba.blog.Entities;


import org.apache.tomcat.jni.Local;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Set;

import static java.lang.Boolean.TRUE;

/**
 * Product entity.
 */
@Entity
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull
    @Size(min = 1,max =20 , message = "rango caracteres 1-20")
    private String title;

    @NotNull
    @Size(min = 1, max =100 , message = "rango caracteres 1-100")
    private String text;

    //@DateTimeFormat(pattern = "dd/MM/yyyy")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private java.sql.Date date;

    @OneToMany(mappedBy = "post")
    private Set<Comment> comments;

    @NotNull
    @Column(columnDefinition ="int(5) default '0'" )
    private Integer likes = 0;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @NotNull
    private String showPost = "SI";

    private String image;

    public Post()
    {
        date = date.valueOf(LocalDate.now());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDate getDate() {
        return date.toLocalDate();
    }

    public void setDate(LocalDate date){
        this.date = Date.valueOf(date);
    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getShowPost() {
        return showPost;
    }

    public void setShowPost(String showPost) {
        this.showPost = showPost;
    }
}