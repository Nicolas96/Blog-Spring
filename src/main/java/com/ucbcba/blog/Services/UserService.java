package com.ucbcba.blog.Services;

import com.ucbcba.blog.Entities.User;

public interface UserService {

    Iterable<User> listAllUsers();

    User getUserById(Integer id);

    User saveUser(User user);

    void deleteUser(Integer id);

}
