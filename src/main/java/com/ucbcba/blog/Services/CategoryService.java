package com.ucbcba.blog.Services;

import com.ucbcba.blog.Entities.Category;

public interface CategoryService {

    Iterable<Category> listAllCategories();

    Category getCategoryById(Integer id);

    Category saveCategory(Category category);

    void deleteCategory(Integer id);
}
