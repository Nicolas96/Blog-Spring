package com.ucbcba.blog.Services;
import com.ucbcba.blog.Entities.Post;

public interface PostService {
    Iterable<Post> listAllPosts();

    Post getPostById(Integer id);

    Post savePost(Post post);

    void deletePost(Integer id);
}
