package com.ucbcba.blog.Services;

import com.ucbcba.blog.Entities.Comment;

public interface CommentService {

    Iterable<Comment> listAllComments();

    Comment getCommentById(Integer id);

    Comment saveComment(Comment comment);

    void deleteComment(Integer id);
}
