package com.ucbcba.blog.Services;

import com.ucbcba.blog.Entities.Category;
import com.ucbcba.blog.Repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl implements CategoryService{

    private CategoryRepository categoryRepository;

    @Autowired
    @Qualifier(value = "categoryRepository")
    public void setCategoryRepository(CategoryRepository categoryRepository)
    {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public Iterable<Category> listAllCategories(){ return categoryRepository.findAll();}

    @Override
    public Category getCategoryById(Integer id){ return categoryRepository.findOne(id);}

    @Override
    public Category saveCategory(Category category){ return  categoryRepository.save(category); }

    @Override
    public void deleteCategory(Integer id){ categoryRepository.delete(id);}
}
